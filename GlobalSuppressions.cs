﻿// This file is used by Code Analysis to maintain SuppressMessage
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given
// a specific target and scoped to a namespace, type, member, etc.

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("CodeQuality", "IDE0052:Quitar miembros privados no leídos", Justification = "<pendiente>", Scope = "member", Target = "~F:API_PRUEBA.Pages.PrivacyModel._logger")]
[assembly: SuppressMessage("CodeQuality", "IDE0052:Quitar miembros privados no leídos", Justification = "<pendiente>", Scope = "member", Target = "~F:API_PRUEBA.Controllers.API_PRUEBA_INSERT._logger")]
[assembly: SuppressMessage("CodeQuality", "IDE0052:Quitar miembros privados no leídos", Justification = "<pendiente>", Scope = "member", Target = "~F:API_PRUEBA.Pages.IndexModel._logger")]
[assembly: SuppressMessage("CodeQuality", "IDE0052:Quitar miembros privados no leídos", Justification = "<pendiente>", Scope = "member", Target = "~F:API_PRUEBA.Pages.ErrorModel._logger")]
