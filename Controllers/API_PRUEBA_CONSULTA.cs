﻿using API_PRUEBA.App_Code.Functions;
using API_PRUEBA.App_Code.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;

namespace API_PRUEBA.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class API_PRUEBA_INSERT : ControllerBase
    {
        public IConfiguration Configuration { get; }
        private readonly ILogger<API_PRUEBA_INSERT> _logger;

        public API_PRUEBA_INSERT(IConfiguration configuration, ILogger<API_PRUEBA_INSERT> logger)
        {
            Configuration = configuration;
            _logger = logger;
        }


        [HttpPost]
        public ResponseService Consulta(RequestService requestService)
        {

            ResponseService responseService = new()
            {
                Status = false,
                Creation_date = DateTime.Now,
                DataService = requestService
            };

            try
            {

                ///Codigo


            }
            catch (Exception ex)
            {
                ResponseError responseError = new()
                {
                    Code = -1000,
                    Description = "Ocurrio un error al insertar el log" + ex.Message
                };
                responseService.Error = responseError;
                return responseService;
            }

            responseService.Status = true;
            return responseService;


        }

    }
}
