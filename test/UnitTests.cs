﻿using API_PRUEBA.App_Code.Model;
using API_PRUEBA.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace API_PRUEBA.test
{
    [TestClass]
    public class MyTests
    {
        [TestMethod]
        public void TestConsulta()
        {
            // Arrange
            var requestService = new RequestService
            {
                IdSolicitud = 123,
                IdUsuario = 1,
                Pantalla = "Pantalla"
            };

            var myClass = new API_PRUEBA_INSERT(null,null); // reemplaza MyClass con la clase que contiene el método Consulta)

            // Act
            var responseService = myClass.Consulta(requestService);

            // Assert
            Assert.IsTrue(responseService.Status);
            Assert.AreEqual("Pantalla", responseService.DataService.Pantalla);

        }
    }
}
