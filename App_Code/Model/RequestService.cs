﻿namespace API_PRUEBA.App_Code.Model
{
    public class RequestService
    {
        public int IdUsuario { get; set; }
        public string Modulo { get; set; }
        public int IdSolicitud { get; set; }
        public string Pantalla { get; set; }
        public string JsonAntiguo { get; set; }
        public string JsonNuevo { get; set; }
    }
}