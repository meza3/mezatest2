﻿using System;

namespace API_PRUEBA.App_Code.Model
{
    public class ResponseService
    {
        public bool Status { get; set; }

        public RequestService DataService { get; set; }

        public DateTime Creation_date { get; set; }

        public ResponseError Error { get; set; }
    }
}