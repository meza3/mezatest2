﻿namespace API_PRUEBA.App_Code.Model
{
    public class ResponseError
    {
        public int Code { get; set; }
        public string Description { get; set; }
    }
}
