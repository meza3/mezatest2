﻿using System;
using System.Collections.Generic;

namespace API_PRUEBA.App_Code.Model
{

    /// <summary>
    /// Descripción breve de ModelUtilities
    /// </summary>
    public static class ModelUtilities
    {
        public static int? GetInt(object obj)
        {
            return obj is null or DBNull ? null : Convert.ToInt32(obj);
        }

        public static int? GetInt(object obj, int defaultValue)
        {
            return obj is null or DBNull ? defaultValue : Convert.ToInt32(obj);
        }

        public static int GetNotNullInt(object obj)
        {
            return obj is null or DBNull ? 0 : Convert.ToInt32(obj);
        }

        public static decimal? GetDecimal(object obj)
        {
            return obj is null or DBNull ? null : Convert.ToDecimal(obj);
        }

        public static decimal? GetDecimal(object obj, decimal defaultValue)
        {
            return obj is null or DBNull ? defaultValue : Convert.ToDecimal(obj);
        }

        public static double? GetDouble(object obj)
        {
            return obj is null or DBNull ? null : Convert.ToDouble(obj);
        }

        public static double? GetDouble(object obj, double defaultValue)
        {
            return obj is null or DBNull ? defaultValue : Convert.ToDouble(obj);
        }

        public static string GetString(object obj)
        {
            return obj is null or DBNull ? string.Empty : obj.ToString();
        }

        public static DateTime? GetDateTime(object obj)
        {
            return obj is null or DBNull ? null : Convert.ToDateTime(obj);
        }

        public static DateTime? GetDateTime(object obj, DateTime defaultValue)
        {
            return obj is null or DBNull ? defaultValue : Convert.ToDateTime(obj);
        }

        public static bool? GetBool(object obj)
        {
            return obj is null or DBNull ? null : Convert.ToBoolean(obj);
        }

        public static bool? GetBool(object obj, bool defaultValue)
        {
            return obj is null or DBNull ? defaultValue : Convert.ToBoolean(obj);
        }

        public static bool GetNotNullBool(object obj)
        {
            return Convert.ToBoolean(obj);
        }

        public static long? GetLong(object obj)
        {
            return obj is null or DBNull ? null : Convert.ToInt64(obj);
        }

        public static Dictionary<string, string> GetTableValueDictionary(object obj)
        {
            Dictionary<string, string> dic = new();
            bool stop = obj is null or DBNull;
            stop = !stop && string.IsNullOrEmpty(obj.ToString());
            if (stop)
            {
                return dic;
            }

            string[] items = obj.ToString().Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            if (items.Length == 0)
            {
                return dic;
            }

            foreach (string item in items)
            {
                string[] values = item.Split(new[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
                if (values.Length <= 1)
                {
                    continue;
                }

                dic.Add(values[0], values[1]);
            }

            return dic;
        }

        public static object SetStringDbNull(string obj)
        {
            return string.IsNullOrEmpty(obj) || obj == " " ? DBNull.Value : obj;
        }

        public static object SetStringEmpty(string obj)
        {
            return obj ?? string.Empty;
        }

        public static object SetLongDbNull(long? obj)
        {
            return obj == null ? DBNull.Value : obj;
        }

        public static object SetIntDbNull(int? obj)
        {
            return obj == null ? DBNull.Value : obj;
        }

        public static object SetDateTimeDbNull(DateTime? obj)
        {
            return obj == null ? DBNull.Value : obj;
        }

        public static object SetDecimalDbNull(decimal? obj)
        {
            return obj == null ? DBNull.Value : obj;
        }
        public static object SetDoublelDbNull(double? obj)
        {
            return obj == null ? DBNull.Value : obj;
        }


        public static object SetBoolDbNull(bool? obj)
        {
            return obj == null ? DBNull.Value : obj;
        }
    }
}